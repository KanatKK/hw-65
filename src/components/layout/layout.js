import React from 'react';
import './layout.css';

const Layout = props => {

    const sendParams = event => {
        const params = new URLSearchParams({key: event.target.id});
        props.history.push({
            pathname: '/pages',
            search: '?' + params.toString()
        });
    };

    const sendAdminParams = event => {
        props.history.push ({
            pathname: '/pages/admin'
        })
    }

    return (
        <div className="container">
            <header>
                <h4 className="heading">Static Pages</h4>
                <nav className="mainNav">
                    <p onClick={sendParams} className="pages" id="home">Home</p>
                    <p onClick={sendParams} className="pages" id="mainPage">Main Page</p>
                    <p onClick={sendParams} className="pages" id="aboutUs">About Us</p>
                    <p onClick={sendParams} className="pages" id="contacts">Contacts</p>
                    <p onClick={sendParams} className="pages" id="reviews">Reviews</p>
                    <p onClick={sendAdminParams} id="admin" style={{cursor: "pointer"}}>Admin</p>
                </nav>
            </header>
        </div>
    );
};

export default Layout;