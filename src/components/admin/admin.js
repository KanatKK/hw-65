import React, {useState} from 'react';
import './admin.css';
import axios from 'axios'

const Admin = props => {
    const [pageInner, setPageInner] = useState([{
        title: '', content: '',
    }]);
    const pageInnerCopy = [...pageInner];

    const [currentPage, setCurrentPage] = useState([{name: 'select'}]);
    const currentPageCopy = [...currentPage];

    const getPageVal = async event => {
        currentPageCopy[0] = {name: event.target.value};
        setCurrentPage(currentPageCopy);
        if (event.target.value !== 'select') {
            const pageVal = await axios.get('https://pages-27e39.firebaseio.com/'+ event.target.value +'.json');
            pageInnerCopy[0] = {
                title: pageVal.data.title,
                content: pageVal.data.content,
            };
            setPageInner(pageInnerCopy);
        } else {
            pageInnerCopy[0] = {
                title: '',
                content: '',
            };
            setPageInner(pageInnerCopy);
        }
    };

    const titleChanger = event => {
        pageInnerCopy[0].title = event.target.value;
        setPageInner(pageInnerCopy);
    };

    const contentChanger = event => {
        pageInnerCopy[0].content = event.target.value;
        setPageInner(pageInnerCopy);
    };

    const editPage = async event => {
        event.preventDefault();
        try {
            await axios.put(
                'https://pages-27e39.firebaseio.com/'+ currentPage[0].name +'.json',
                {...pageInner[0]}
                );
        } finally {
            const params = new URLSearchParams({key: currentPage[0].name})
            props.history.push({
                pathname: '/pages',
                search: '?' + params.toString(),
            });
        }
    };

    const disabled = currentPage[0].name === "select"

    return (
        <div className="container">
            <form className="editPages" onSubmit={editPage}>
                <h1>Edit pages</h1>
                <p>Select page:</p>
                <select name="selectPage" onClick={getPageVal}>
                    <option value="select">Select</option>
                    <option value="home">Home</option>
                    <option value="mainPage">Main Page</option>
                    <option value="aboutUs">About Us</option>
                    <option value="contacts">Contacts</option>
                    <option value="reviews">Reviews</option>
                </select>
                <p className="txt">Title:</p>
                <input
                    type="text" className="inputForTitle"
                    value={pageInner[0].title} onChange={titleChanger}
                    disabled={disabled}
                />
                <p className="txt">Content:</p>
                <textarea
                    name="forContent" className="areaForContent"
                    value={pageInner[0].content} onChange={contentChanger}
                    disabled={disabled}
                />
                <button type="submit" disabled={disabled}>Save</button>
            </form>
        </div>
    );
};

export default Admin;