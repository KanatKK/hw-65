import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Layout from "../../components/layout/layout";
import './App.css';
import Page from "../../components/page/page";
import Admin from "../../components/admin/admin";

const App = () => {
    return (
        <BrowserRouter>
            <Route component={Layout}/>
            <Switch>
                <Route path="/pages/admin" exact component={Admin}/>
                <Route path="/" component={Page} />
            </Switch>
        </BrowserRouter>
    );
};

export default App;