import React, {useEffect, useState} from 'react';
import './page.css';
import axios from 'axios';

const Page = props => {
    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };

    const [pageInner, setPageInner] = useState([]);
    const pageInnerCopy = [...pageInner];

    useEffect(() => {
        const fetchData = async () => {
            const pages = await axios.get('https://pages-27e39.firebaseio.com/'+ getParams().key +'.json');
            pageInnerCopy[0] = {content: pages.data.content, title: pages.data.title};
            setPageInner(pageInnerCopy);
        };
        fetchData();
    }, [getParams().key]);

    if (pageInner[0] !== undefined) {
        return (
            <div className="container">
                <div className="page">
                    <h1 className="title">{pageInner[0].title}</h1>
                    <p className="content">{pageInner[0].content}</p>
                </div>
            </div>
        );
    } else {
        return null
    }
};

export default Page;